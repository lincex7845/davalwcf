CREATE Procedure [dbo].[InfoTarjeta]
(
	@token_session nvarchar(Max),
	@token_POS nvarchar(Max),
	@uid_card nvarchar(Max)
)
AS 
BEGIN
 Declare @s int;
 Declare @p int;
 Declare @serial nvarchar(max);
 Set @serial = @uid_card;
 BEGIN TRY
	
	-- Validar POS
	Set @p = (Select count(idPOS) from POS where 
	POS.token =  @token_POS and POS.idEstado in (Select idEstado from Estado where Estado.descripcion like 'Activo'));

	IF @p > 0
		Begin
			--Validar Sesion
			Set @s = (Select count(idHistSesion) from HistorialSesion where
			HistorialSesion.token = @token_session and HistorialSesion.idEstado in 
			(Select idEstado from Estado where Estado.descripcion like 'En Linea'));
			IF @S > 0
					Select 200 as Codigo, Cliente.nombre, Cliente.apellido, Tarjeta.contraseña,  Estrato.estratoNom, Estrato.valor
					from Cliente, ClienteTarjeta, Tarjeta, Estrato
					where Cliente.cedula = ClienteTarjeta.cliente and ClienteTarjeta.tarjeta = Tarjeta.idTarjeta and
					Cliente.idEstrato = Estrato.idEstrato and Tarjeta.serial =  @serial;
			ELSE
				Select 300 as Codigo;
		End
	ELSE
		Select 300 as Codigo;
 END TRY
 BEGIN CATCH
	--SELECT 
	--		ERROR_NUMBER() AS ErrorNumber
	--		,ERROR_SEVERITY() AS ErrorSeverity
	--		,ERROR_STATE() AS ErrorState
	--		,ERROR_PROCEDURE() AS ErrorProcedure
	--		,ERROR_LINE() AS ErrorLine
	--		,ERROR_MESSAGE() AS ErrorMessage;

	--	IF @@TRANCOUNT > 0		--Cuenta el número de transacciones
	--		Begin
	--			ROLLBACK TRANSACTION;
	--			RETURN 300
	--		End
	--	ELSE
	--		RETURN 500
 END CATCH;
END