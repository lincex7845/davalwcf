﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace WCFServiceWebRole1
{
    [DataContract]
    public class InInfoCard
    {
        [DataMember]
        public string token_session;
        [DataMember]
        public string token_poas;
        [DataMember]
        public string uid_card;
    }

}
