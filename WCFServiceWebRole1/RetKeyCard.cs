﻿using System.Runtime.Serialization;

namespace WCFServiceWebRole1
{
    [DataContract]
    public class RetKeyCard
    {
        [DataMember]
        public int code_status { get; set; }
        [DataMember]
        public string password { get; set; }
    }
}
