﻿using System;
using System.Runtime.Serialization;

namespace WCFServiceWebRole1
{
    [DataContract]
    public class RetInfoUser
    {
        [DataMember]
        public int code_status;
        [DataMember]
        public string token_session;
        [DataMember]
        public int re;
        [DataMember]
        public int timeout;
        [DataMember]
        public string nombre_usuario;
        [DataMember]
        public string apellido_usuario;
        [DataMember]
        public string direccion_POAS;
        [DataMember]
        public string nombre_POAS;
        [DataMember]
        public int recargas_mes;
        [DataMember]
        public DateTime anterior_accesso;
    }
}
