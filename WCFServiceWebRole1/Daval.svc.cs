﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using WCFServiceWebRole1.DavalDS2TableAdapters;

namespace WCFServiceWebRole1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Daval : IDaval
    {

        //public List<Estrato> getEstratos()
        //{
        //    DavalDS.EstratoDataTable EDT = new DavalDS.EstratoDataTable();
        //    EstratoTableAdapter ETA = new EstratoTableAdapter();
        //    List<Estrato> estratos = new List<Estrato>();

        //    EDT.Clear();
        //    ETA.Fill(EDT);

        //    foreach (DavalDS.EstratoRow r in EDT.Rows)
        //    {
        //        var estrato = new Estrato
        //        {
        //            idEstrato = (int)r.idEstrato,
        //            estratoNom = r.estratoNom.ToString(),
        //            valor = (int)r.valor
        //        };
        //        estratos.Add(estrato);
        //    }
        //    return estratos;
        //}

        //public Estrato getEstratoById(string id)
        //{
        //    DavalDS.getEstratoByIdDataTable EDT = new DavalDS.getEstratoByIdDataTable();
        //    getEstratoByIdTableAdapter ETA = new getEstratoByIdTableAdapter();
        //    Estrato es = new Estrato();

        //    int idEstrato = 0;
        //    idEstrato = int.Parse(id);

        //    EDT.Clear();
        //    ETA.Fill(EDT, idEstrato);

        //    foreach (DavalDS.getEstratoByIdRow r in EDT.Rows)
        //    {
        //        es.idEstrato = (int)r.idEstrato;
        //        es.estratoNom = r.estratoNom.ToString();
        //        es.valor = (int)r.valor;
        //    }
        //    return es;
        //}


        public int[] AutenticarPOAS(string poa, string token)
        {
            int[] returna = new int[1];
            if (poa.Equals("POS"))
            {
                AutenticarPOSTableAdapter APOSTA = new AutenticarPOSTableAdapter();
                returna[0] = int.Parse(APOSTA.AutenticarPOSCodigo(token).ToString());
            }

            else if (poa.Equals("POA"))
            {
                DavalDS2.AutenticarPOADataTable APOA = new DavalDS2.AutenticarPOADataTable();
                AutenticarPOATableAdapter APOATA = new AutenticarPOATableAdapter();

                APOA.Clear();
                APOATA.Fill(APOA, token);

                foreach (DavalDS2.AutenticarPOARow r in APOA.Rows)
                    returna[0] = (int)r.resultado;
            }

            return returna;
        }

        public RetInfoCard Requestinfocard(string token_session, string token_poas, string uid_card)
        {
            DavalDS2.InfoTarjetaDataTable ITDT = new DavalDS2.InfoTarjetaDataTable();
            InfoTarjetaTableAdapter ITTA = new InfoTarjetaTableAdapter();

            RetInfoCard retinfocard = new RetInfoCard();
            ITDT.Clear();
            ITTA.Fill(ITDT, token_session, token_poas, uid_card);

            foreach (DavalDS2.InfoTarjetaRow r in ITDT.Rows)
            {
                retinfocard.code_status = (int)r.Codigo;
                retinfocard.nombre_propietario_tarjeta = (string)r.nombre;
                retinfocard.apellido_propietario_tarjeta = (string)r.apellido;
                retinfocard.password_tarjeta = (string)r.contraseña;
                retinfocard.estrato = (string)r.estratoNom;
                retinfocard.valor_estrato = (int)r.valor;
            }
            return retinfocard;
        }

        public RetInfoUser AutenticarUser(string type, string username, string password, string token)
        {
            
            LoginTableAdapter LPOSTA = new LoginTableAdapter();
            RetInfoUser InfoUser = new RetInfoUser();
            if (LPOSTA.ValidarUsuario(username, password) == 1)
            {

                if (type.Equals("POS"))
                {

                    LPOSTA.LoginPOS(username, password, token);
                    RLoginPOSTableAdapter RLPOSTA = new RLoginPOSTableAdapter();
                    DavalDS2.RLoginPOSDataTable RLPOSDT = new DavalDS2.RLoginPOSDataTable();
                    RLPOSDT.Clear();
                    RLPOSTA.Fill(RLPOSDT);
                    foreach (DavalDS2.RLoginPOSRow r in RLPOSDT.Rows)
                    {
                        InfoUser.code_status = (int)r.Codigo;
                        InfoUser.token_session = r.Token_Sesion.ToString();
                        InfoUser.re = (int)r.RE;
                        InfoUser.nombre_usuario = r.Nombre_Usuario.ToString();
                        InfoUser.apellido_usuario = r.Apellido_Usuario.ToString();
                        InfoUser.direccion_POAS = r.Direccion_POAS.ToString();
                        InfoUser.nombre_POAS = r.Nombre_POAS.ToString();
                        InfoUser.recargas_mes = (int)r.Cant_Recargas;
                        InfoUser.anterior_accesso = (DateTime)r.Ultimo_Acceso;
                        InfoUser.timeout = (int)r.Time_Out;
                    }
                }

                if (type.Equals("POA"))
                {

                    LPOSTA.LoginPOA(username, password, token);
                    RLoginPOSTableAdapter RLPOSTA = new RLoginPOSTableAdapter();
                    DavalDS2.RLoginPOSDataTable RLPOSDT = new DavalDS2.RLoginPOSDataTable();
                    RLPOSDT.Clear();
                    RLPOSTA.Fill(RLPOSDT);
                    foreach (DavalDS2.RLoginPOSRow r in RLPOSDT.Rows)
                    {
                        InfoUser.code_status = (int)r.Codigo;
                        InfoUser.token_session = r.Token_Sesion.ToString();
                        InfoUser.re = (int)r.RE;
                        InfoUser.nombre_usuario = r.Nombre_Usuario.ToString();
                        InfoUser.apellido_usuario = r.Apellido_Usuario.ToString();
                        InfoUser.direccion_POAS = r.Direccion_POAS.ToString();
                        InfoUser.nombre_POAS = r.Nombre_POAS.ToString();
                        InfoUser.recargas_mes = (int)r.Cant_Recargas;
                        InfoUser.anterior_accesso = (DateTime)r.Ultimo_Acceso;
                        InfoUser.timeout = (int)r.Time_Out;
                    }
                }
            }

            else
                InfoUser.code_status = 300;

            return InfoUser;
        }

        public int[] Recargar(string token_session, string token_poas, string valor, string metros, string uid_card)
        {
            int[] codigo = new int[1];
            DavalDS2.RecargarDataTable RDT = new DavalDS2.RecargarDataTable();
            RecargarTableAdapter RTA = new RecargarTableAdapter();
            RDT.Clear();
            RTA.Fill(RDT, token_session, token_poas, float.Parse(valor), float.Parse(metros), uid_card);
            foreach (DavalDS2.RecargarRow r in RDT.Rows)
                codigo[0] = (int)r.codigo;
            return codigo;
        }


        //public int[] Registrycard(string uid, string token_session, string token_poas)
        //{
        //    throw new NotImplementedException();
        //}

        public int[] Registrymeter(string serial, string token_session, string token_poas)
        {
            int[] codigo = new int[1];
            DavalDS2.ActivarMedidorDataTable AMDT = new DavalDS2.ActivarMedidorDataTable();
            ActivarMedidorTableAdapter AMTA = new ActivarMedidorTableAdapter();
            AMDT.Clear();
            AMTA.Fill(AMDT, serial, token_session, token_poas);
            foreach (DavalDS2.ActivarMedidorRow r in AMDT.Rows)
                codigo[0] = (int)r.codigo;
            return codigo;
        }

        public int[] Changepassword(string older_password, string new_password, string token_session, string token_poas)
        {
            int[] codigo = new int[1];
            DavalDS2.CambiarPassUsuDataTable CPUDT = new DavalDS2.CambiarPassUsuDataTable();
            CambiarPassUsuTableAdapter CPUTA = new CambiarPassUsuTableAdapter();
            CPUDT.Clear();
            CPUTA.Fill(CPUDT, older_password, new_password, token_session, token_poas);
            foreach(DavalDS2.CambiarPassUsuRow r in CPUDT.Rows)
                codigo[0] = (int)r.codigo;
            return codigo;
               

        }

        public RetKeyCard Keycard(string uid_card, string token_session, string token_poas)
        {
            DavalDS2.KeyTarjetaDataTable KTDT = new DavalDS2.KeyTarjetaDataTable();
            KeyTarjetaTableAdapter KTTA = new KeyTarjetaTableAdapter();

            RetKeyCard retKeyCard = new RetKeyCard();

            KTDT.Clear();
            KTTA.Fill(KTDT, uid_card, token_session, token_poas);
            foreach (DavalDS2.KeyTarjetaRow r in KTDT.Rows)
            {
                retKeyCard.code_status = (int)r.codigo;
                retKeyCard.password = (string) r.contraseña;
            }

            return retKeyCard;
           
        }
    }
}
