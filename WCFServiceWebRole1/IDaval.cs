﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;

namespace WCFServiceWebRole1
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IDaval
    {

        //[OperationContract]
        //string GetData(int value);

        //[OperationContract]
        //CompositeType GetDataUsingDataContract(CompositeType composite);

        // TODO: Add your service operations here

        //[OperationContract]
        //[WebGet(UriTemplate = ("Estratos/"), ResponseFormat = WebMessageFormat.Json)]
        //List<Estrato> getEstratos();

        //[OperationContract]
        //[WebGet(UriTemplate = ("Estratos/Details/{id}"), ResponseFormat = WebMessageFormat.Json)]
        //Estrato getEstratoById(string id);

        //[OperationContract]
        //[WebInvoke(UriTemplate = ("Estratos/Cliente/{id}"), 
        //    RequestFormat = WebMessageFormat.Json, 
        //    ResponseFormat = WebMessageFormat.Json))]

        [OperationContract]
        [WebGet(UriTemplate = ("AutenticarPOAS/{poa}/{token}"),
            //RequestFormat = WebMessageFormat.Json,
            ResponseFormat = WebMessageFormat.Json )]
        int[] AutenticarPOAS(string poa, string token);

        [OperationContract]
        [WebGet(UriTemplate = ("Requestinfocard/{token_session}/{token_poas}/{uid_card}"), ResponseFormat = WebMessageFormat.Json)]
        RetInfoCard Requestinfocard(string token_session, string token_poas, string uid_card);
        
        [OperationContract]
        [WebGet(UriTemplate = ("AutenticarUser/{type}/{username}/{password}/{token}"), 
            ResponseFormat = WebMessageFormat.Json)]
        RetInfoUser AutenticarUser (string type, string username, string password, string token);

        [OperationContract]
        [WebGet(UriTemplate = ("Recargar/{token_session}/{token_poas}/{valor}/{metros}/{uid_card}"), 
            ResponseFormat = WebMessageFormat.Json)]
        int[] Recargar(string token_session, string token_poas, string valor, string metros, string uid_card);

        //[OperationContract]
        //[WebGet(UriTemplate = ("Registrycard/{uid}/{token_session}/{token_poas}"), 
        //    ResponseFormat = WebMessageFormat.Json)]
        //int[] Registrycard(string uid, string token_session, string token_poas);

       
        [OperationContract]
        [WebGet(UriTemplate = ("Registrymeter/{serial}/{token_session}/{token_poas}"), 
            ResponseFormat = WebMessageFormat.Json)]
        int[] Registrymeter(string serial, string token_session, string token_poas); 

        [OperationContract]
        [WebGet(UriTemplate = ("Changepassword/{older_password}/{new_password}/{toke_session}/{token_poas}"), 
            ResponseFormat = WebMessageFormat.Json)]
        int[] Changepassword(string older_password, string new_password, string toke_session, string token_poas); 

        [OperationContract]
        [WebGet(UriTemplate = ("Keycard/{uid_card}/{token_session}/{token_poas}"), 
            ResponseFormat = WebMessageFormat.Json)]
        RetKeyCard Keycard(string uid_card, string token_session, string token_poas); 
        
        
    }


    //// Use a data contract as illustrated in the sample below to add composite types to service operations.
    //[DataContract]
    //public class CompositeType
    //{
    //    bool boolValue = true;
    //    string stringValue = "Hello ";

    //    [DataMember]
    //    public bool BoolValue
    //    {
    //        get { return boolValue; }
    //        set { boolValue = value; }
    //    }

    //    [DataMember]
    //    public string StringValue
    //    {
    //        get { return stringValue; }
    //        set { stringValue = value; }
    //    }
    //}
}
