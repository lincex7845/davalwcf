﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCFServiceWebRole1
{
    [DataContract]
    public class RetLogin
    {

        [DataMember]
        public int code_status;
        [DataMember]
        public string token_session;
        [DataMember]
        public float RE;
        [DataMember]
        public int timeout;
        [DataMember]
        public string nombre_usuario;
        [DataMember]
        public string apellido_usuario;
        [DataMember]
        public string direccion_usuario;
        [DataMember]
        public string telefono_usuario;
        [DataMember]
        public string direccion_POAS;
        [DataMember]
        public string Nombre_POAS;
        [DataMember]
        public int recargas_mes;
        [DataMember]
        public DateTime anterior_accesso;
    }
}