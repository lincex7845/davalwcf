﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Web;

namespace WCFServiceWebRole1
{
    [DataContract]
    public class RetInfoCard
    {
       [DataMember]
        public int code_status;
       [DataMember]
       public string nombre_propietario_tarjeta;
       [DataMember]
       public string apellido_propietario_tarjeta;
       [DataMember]
       public string password_tarjeta;
       [DataMember]
       public string estrato;
       [DataMember]
       public int valor_estrato;
    }
}