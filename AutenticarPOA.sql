CREATE Procedure [dbo].[AutenticarPOA]
(
	@toekn nvarchar(100)
)
AS
BEGIN
	--BEGIN TRY
		Declare @estado int;
		Select @estado = idEstado from Estado where Estado.descripcion like 'Activo';
		Declare @resultado int;
		Update POA set fecha_reg =  CURRENT_TIMESTAMP, idEstado = @estado
		where POA.token = @toekn and idEstado in (Select idEstado from Estado where Estado.descripcion like 'Inactivo');
		IF @@ROWCOUNT = 0	--Cuenta el número de filas afectadas
			Set @resultado = 300;
		ELSE 
			Set @resultado = 200;
	--END TRY
	--BEGIN CATCH
	--	SELECT 
	--		ERROR_NUMBER() AS ErrorNumber
	--		,ERROR_SEVERITY() AS ErrorSeverity
	--		,ERROR_STATE() AS ErrorState
	--		,ERROR_PROCEDURE() AS ErrorProcedure
	--		,ERROR_LINE() AS ErrorLine
	--		,ERROR_MESSAGE() AS ErrorMessage
	--		,@resultado as resultado;

		--IF @@TRANCOUNT > 0		--Cuenta el número de transacciones
		--	Begin
		--		--ROLLBACK TRANSACTION;
		--		Set @resultado = 300;
		--	End
		--ELSE
		--	Set @resultado = 500;
	--END CATCH;
	Select @resultado as resultado;
END
GO